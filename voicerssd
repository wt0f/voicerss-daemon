#!/usr/bin/env python3

import os
import sys
import configparser
import voicerss_tts
import argparse
import daemon
import time
import logging

# import the appropriate hashing function for this system
import struct
if struct.calcsize("P")*8 == 64:
    from hashlib import blake2b as voice_digest
else:
    from hashlib import blake2s as voice_digest


def do_main_program(conf):
    print("Running server code")
    for i in range(0,15,5):
        print(i)
        time.sleep(5)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--config', '-c', metavar='FILE',
        default='/etc/voicerrs.ini',
        help='Load config file (default: /etc/voicerss.ini')
    parser.add_argument('--daemon', '-D', action='store_true',
        help='Run a voicerss server')
    parser.add_argument('--foreground', '-F', action='store_true',
        help='Do not daemonize the server and leave running in foreground')
    parser.add_argument('--voice', '-v', metavar='NAME',
        help='Use the named voice setting from config file')
    parser.add_argument('--file', '-f', metavar='FILENAME',
        help='Read text from specified file')
    args = parser.parse_args()

    # process the configuration file
    conf = configparser.ConfigParser()
    conf.read(args.config)

    # ensure the cache directory exists
    if not os.path.isdir(conf['global']['cache_dir']):
        try:
            os.makedirs(conf['global']['cache_dir'])
        except PermissionError:
            logging.critical("Failure making cache directory: {}".format(conf['global']['cache_dir']))
            sys.exit(1)

    if args.daemon:
        # initialize the logging context
        logging.basicConfig(filename=conf['global']['logfile'],
                            level=conf['global']['log_level'])

        with daemon.DaemonContext():
            do_main_program(conf)
    else:
        logging.info('running locally')
        do_main_program(conf)


    print("normal exit")
    sys.exit(0)
